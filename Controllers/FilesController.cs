using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Readers.Application;

namespace Readers.Controllers
{
    public class FilesController : ControllerBase
    {
        private readonly ReadingService _readingService;
        
        public FilesController(ReadingService readingService)
        {
            _readingService = readingService ?? throw new ArgumentNullException(nameof(readingService));
        }
        
        [HttpGet, Route("api/files")]
        public async Task<string> ReadFile([FromQuery] string filename, CancellationToken cancellationToken = default)
        {
            return await _readingService.ReadFile(filename, cancellationToken);
        }
    }
}