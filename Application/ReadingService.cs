using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Readers.Application
{
    public class ReadingService
    {
        private const int ReadTimeEmulationSeconds = 2;
        private const string WorkFolder = "content root";

        private static readonly ConcurrentDictionary<string, FileReadingProcess> FileReadingProcesses = new();
        
        public async Task<string> ReadFile(string fileName, CancellationToken cancellationToken)
        {
            if (fileName is null)
            {
                throw new ArgumentNullException(nameof(fileName));
            }
            
            var fullPathFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, WorkFolder, fileName);

            if (!File.Exists(fullPathFileName))
            {
                throw new FileNotFoundException($"File: {fullPathFileName} not found!");
            }

            var fileReadingProcess = FileReadingProcesses.GetOrAdd(fullPathFileName, new FileReadingProcess());
            
            if (await ShouldBeRead(fileReadingProcess, cancellationToken))
            {
                try
                {
                    fileReadingProcess.SuccessfulReadAttemptFlag = true;
                    await Task.Delay(TimeSpan.FromSeconds(ReadTimeEmulationSeconds), cancellationToken);
                    fileReadingProcess.SetReadResult(fileName);
                    return fileReadingProcess.ReadResult;
                }
                catch (Exception)
                {
                    fileReadingProcess.SuccessfulReadAttemptFlag = false;
                    // logging
                }
                finally
                {
                    fileReadingProcess.SemaphoreWriter.Release();
                }
            }
            
            Interlocked.Increment(ref fileReadingProcess.AwaitingReadersCount);
            
            await fileReadingProcess.SemaphoreReader.WaitAsync(cancellationToken);
            try
            {
                while (IsFileReading(fileReadingProcess, cancellationToken))
                {
                    //waiting for the file to finish reading
                }

                var readResult = fileReadingProcess.ReadResult;
                
                fileReadingProcess.AwaitingReadersCount--;
                
                if (fileReadingProcess.AwaitingReadersCount == 0)
                {
                    fileReadingProcess.SuccessfulReadAttemptFlag = false;
                    fileReadingProcess.ResetReadResult();
                };
                
                cancellationToken.ThrowIfCancellationRequested();
                
                return readResult;
            }
            finally
            {
                fileReadingProcess.SemaphoreReader.Release();   
            }
        }

        private static async Task<bool> ShouldBeRead(FileReadingProcess fileReadingProcess, CancellationToken cancellationToken)
        {
            return !fileReadingProcess.SuccessfulReadAttemptFlag &&
                   await fileReadingProcess.SemaphoreWriter.WaitAsync(0, cancellationToken);
        }

        private static bool IsFileReading(FileReadingProcess fileReadingProcess, CancellationToken cancellationToken)
        {
            return fileReadingProcess.SemaphoreWriter.CurrentCount == 0 && 
                   !cancellationToken.IsCancellationRequested;
        }
    }
}