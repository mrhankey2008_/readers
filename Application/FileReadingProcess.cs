using System.Threading;

namespace Readers.Application
{
    public class FileReadingProcess
    {
        public bool SuccessfulReadAttemptFlag { get; set; }
        
        public int AwaitingReadersCount = 0;
        
        public readonly SemaphoreSlim SemaphoreWriter = new(1, 1);
        
        public readonly SemaphoreSlim SemaphoreReader = new(1, 1);
        
        public string ReadResult { get; private set; }
        
        public void SetReadResult(string fileName)
        {
            ReadResult = fileName;
        }
        
        public void ResetReadResult()
        {
            ReadResult = default;
        }
    }
}